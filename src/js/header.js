"use strict"

const burgerMenu = document.querySelector('.header__burger-menu');
const headerNav = document.querySelector('.header__nav');
const headerBtn = document.querySelectorAll('.header__btn');
const burgerMenuClick = document.querySelector('.header__btn--press');


burgerMenu.addEventListener('click', ev => {
    headerNav.classList.toggle('drop-down');
    headerBtn.forEach(el => {
        el.classList.toggle('js-active');     
    })
})